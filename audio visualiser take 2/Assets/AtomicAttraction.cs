﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtomicAttraction : MonoBehaviour {
    public GameObject _atom, _attractor;
    public Gradient _gradient;
    public int[] _attractPoints;
    public Vector3 _spacingDirection;
    [Range(0,20)]
    public float _spacingBetweenAttractPoints;
    [Range(0, 20)]
    public float _scaleAttractPoints;
    GameObject[] _attractorArray, _atomArray;
    [Range(0,64)]
    public int _amountOfAtomsPerPoint;
    public Vector2 _atomScaleMinMax;
    float[] _atomScaleSet;
    public float _strengthOfAttraction, _maxMagnitude, _randomPosDistance;
    public bool _useGravity;

    private void OnDrawGizmos()
    {
        for (int i = 0; i < _attractPoints.Length; i++)
        {
            float evaluateStep = 0.125f;
            Color color = _gradient.Evaluate(Mathf.Clamp(evaluateStep * _attractPoints[i],0,7));
            Gizmos.color = color;

            Vector3 pos = new Vector3(transform.position.x + (_spacingBetweenAttractPoints * i * _spacingDirection.x),
                transform.position.y + (_spacingBetweenAttractPoints * i * _spacingDirection.y),
                transform.position.z + (_spacingBetweenAttractPoints * i * _spacingDirection.z));
            Gizmos.DrawSphere(pos, _scaleAttractPoints);
        }
    }
    // Use this for initialization
    void Start()
    {
        _attractorArray = new GameObject[_attractPoints.Length];
        _atomArray = new GameObject[_attractPoints.Length * _amountOfAtomsPerPoint];
        _atomScaleSet = new float[_attractPoints.Length * _amountOfAtomsPerPoint];

        int _countAtom = 0;
        //instantiate attract points
        for (int i = 0; i < _attractPoints.Length; i++)
        {

            GameObject _attractorInstance = (GameObject)Instantiate(_attractor);
            _attractorArray[i] = _attractorInstance;

            _attractorInstance.transform.position = new Vector3(transform.position.x + (_spacingBetweenAttractPoints * i * _spacingDirection.x),
             transform.position.y + (_spacingBetweenAttractPoints * i * _spacingDirection.y),
             transform.position.z + (_spacingBetweenAttractPoints * i * _spacingDirection.z));

            _attractorInstance.transform.parent = this.transform;
            _attractorInstance.transform.localScale = new Vector3(_scaleAttractPoints, _scaleAttractPoints, _scaleAttractPoints);
            //instantiate atoms
            for (int j = 0; j < _amountOfAtomsPerPoint; j++)
            {
                GameObject atomInstance = (GameObject)Instantiate(_atom);
                _atomArray[_countAtom] = atomInstance;
                atomInstance.GetComponent<AttractTo>()._attractedTo = _attractorArray[i].transform;
                atomInstance.GetComponent<AttractTo>()._strengthOfAttraction = _strengthOfAttraction;
                atomInstance.GetComponent<AttractTo>()._maxMagnitude = _maxMagnitude;
                if (_useGravity)
                {
                    atomInstance.GetComponent<Rigidbody>().useGravity = true;
                }
                else
                {
                    atomInstance.GetComponent<Rigidbody>().useGravity = false;
                }
                atomInstance.transform.position = new Vector3(
                _attractorArray[i].transform.position.x + Random.Range(-_randomPosDistance, _randomPosDistance),
                _attractorArray[i].transform.position.y + Random.Range(-_randomPosDistance, _randomPosDistance),
                _attractorArray[i].transform.position.z + Random.Range(-_randomPosDistance, _randomPosDistance));

                float randomScale = Random.Range(_atomScaleMinMax.x, _atomScaleMinMax.y);
                _atomScaleSet[_countAtom] = randomScale;
                atomInstance.transform.localScale = new Vector3(_atomScaleSet[_countAtom], _atomScaleSet[_countAtom], _atomScaleSet[_countAtom]);

                atomInstance.transform.parent = transform.parent;

                _countAtom++;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
