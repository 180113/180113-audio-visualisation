﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderValues : MonoBehaviour {
	public colorwall _colorwall;
	public GameObject _lights;
	Slider _slider;
	Light[] _spotlight = new Light[6];
	Light _directionalLight;

	public void SliderDiffuseIntensity()
	{
		_colorwall._diffuseColorMultiplier = _slider.value;
	}

	public void SliderEmissiveIntensity()
	{
		_colorwall._emissionColorMultiplier = _slider.value;
	}

	public void SliderEmissionTreshold()
	{
		_colorwall._treshold = _slider.value;
		_colorwall._tresholdBallSpawn = _slider.value;
	}

	public void SliderBallEmission()
	{
		_colorwall._ballEmissionMultiplier = _slider.value;
	}

	public void SliderMinTimeScale()
	{
		_colorwall._minTimeScale = _slider.value;
	}

	public void SliderMaxTimeScale()
	{
		_colorwall._audioTimeScale = _slider.value;
	}
	public void SliderSpotlights()
	{
		for (int i = 0; i < 6; i++) {
			_spotlight [i].intensity = _slider.value;
		}
	}

	public void SliderDirectionalLight()
	{
		_directionalLight.intensity = _slider.value;
	}

	// Use this for initialization
	void Start () {
		if (_lights != null) {

			for (int i = 0; i < 6; i++) {
				_spotlight[i] = _lights.transform.GetChild (i).GetComponent<Light> ();
			}
			_directionalLight = _lights.transform.GetChild (6).GetComponent<Light> ();
		}
		_slider = GetComponent<Slider> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
