﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleOn_Amplitude : MonoBehaviour
{
    // Start is called before the first frame update
    public int _band;
    public AudioPeer audioPeer;
    public float _startScale, _maxScale;
    public bool _useBuffer;
    Material material;
    public float _red,_green,_blue;

    void Start () {
        material = GetComponent<MeshRenderer>().materials[0];
    }

    // Update is called once per frame
    void Update () {
        if (_useBuffer) {
            Debug.Log("audio Band buffer size :" + audioPeer._audioBandBuffer[_band]);
            //Debug.Log("_band : "+ _band);
            transform.localScale = new Vector3 ((audioPeer._Amplitude*_maxScale)+ _startScale, (audioPeer._Amplitude*_maxScale)+ _startScale, (audioPeer._Amplitude*_maxScale)+ _startScale);
            Color _color =new Color(_red * audioPeer._Amplitude,_green * audioPeer._Amplitude,_blue * audioPeer._Amplitude);
            material.SetColor("_EmissionColor", _color);
        }
        if (!_useBuffer) {

            transform.localScale = new Vector3 ((audioPeer._AmplitudeBuffer*_maxScale)+ _startScale, (audioPeer._AmplitudeBuffer*_maxScale)+ _startScale, (audioPeer._AmplitudeBuffer*_maxScale)+ _startScale);
             Color _color =new Color(_red * audioPeer._AmplitudeBuffer,_green * audioPeer._AmplitudeBuffer,_blue * audioPeer._AmplitudeBuffer);
            material.SetColor("_EmissionColor", _color);
        }
    }
}
