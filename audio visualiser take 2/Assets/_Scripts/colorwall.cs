﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colorwall : MonoBehaviour {

	public AudioPeer _audioPeer; //reference to audiopeer script
	Transform[] _lineTransform = new Transform[64];
	public Transform _spawnPosBalls;
	public Gradient _colorGradient;
	Color[] _color = new Color[64];
	Material[] _materialLine = new Material[64];
	public Material _materialsourceLine;
	Material[] _materialBall = new Material[64];
	public Material _materialsourceBall;
	public float _diffuseColorMultiplier , _emissionColorMultiplier;
	public float _treshold, _tresholdBallSpawn;
	public float _ballEmissionMultiplier;
	public GameObject _ball;
	 List<GameObject> _ballsPool;
	List <MeshRenderer> _ballMeshRenderer;
	List<Rigidbody> _ballRigidbody;
	public int _pooledAmount;

	float[] _intervalBall = new float[64];
	public float _ballIntervalTime;
	public bool spawnballs;
	//object pool
	public float _minTimeScale, _audioTimeScale;



	// Use this for initialization
	void Start () {
		
		_ballsPool = new List<GameObject> ();
		_ballMeshRenderer = new List<MeshRenderer> ();
		_ballRigidbody = new List<Rigidbody> ();

		for (int i = 0; i < _pooledAmount; i++) { // add the amount of balls to the object pool
			GameObject obj = (GameObject)Instantiate (_ball); //instantiate the specified prefab
			obj.SetActive(false); //set every ball to inactive at start
			_ballsPool.Add (obj); //add the ball object to the list
			_ballMeshRenderer.Add (obj.GetComponent<MeshRenderer> ()); //add reference to meshrenderer of the ball to the list
			_ballRigidbody.Add (obj.GetComponent<Rigidbody> ()); //add reference to rigidbody of the ball to the list
		}


		for (int i = 0; i < 64; i++) {
			_intervalBall [i] = 5; //avoid spawning balls without profile
			_materialLine[i] = new Material(_materialsourceLine); //create materials for each colored line
			_materialBall [i] = new Material (_materialsourceBall); // create materials for each colored row of balls
			_color[i] = _colorGradient.Evaluate((1f / 64f) * i); // set colours to the gradient, dividing the gradient by the number of rows. You can change the gradient in the inspector

			this.transform.GetChild (i).GetComponent<MeshRenderer> ().material = _materialLine [i]; //set corresponding materials to all children of the object this script runs on
			_lineTransform[i] = this.transform.GetChild (i).gameObject.transform; // store the position of each line in worldspace, so the balls can spawn at their position

		}
	}

    bool _setTimeScale;
	// Update is called once per frame
	void Update () {


        if (_setTimeScale) { Time.timeScale = _minTimeScale + (_audioPeer._AmplitudeBuffer * _audioTimeScale); } else { _setTimeScale = true; } //changing timescale, makes everything on deltatime including physics go faster/slower
  
       
		for (int i = 0; i < 64; i++) {

			_materialBall[i].SetColor("_EmissionColor", _color[i] * _ballEmissionMultiplier); //update the emission amount of the balls, based on the slider value of the canvas

			if (spawnballs) { //if the boolean is checked that we want to spawn balls
				if ((_audioPeer._audioBand64 [i] > _tresholdBallSpawn) && (_intervalBall [i] <= 0)) { // for each audioband that is between 0-1 we check if the amplitude is higher than the treshold, and then spawn. The interval makes sure we have control over how often balls spawn
					//spawn
					for (int g = 0; g < _ballsPool.Count; g++) {
						if (!_ballsPool [g].activeInHierarchy) { //checking our object pool for an inactive object
							_ballsPool [g].transform.position = new Vector3 (_spawnPosBalls.position.x, _spawnPosBalls.position.y, _lineTransform [i].position.z); // set it's position to the spawnballs transform, and the Z axis of the corresponding line
							_ballMeshRenderer [g].material = _materialBall [i]; // set it's material to the correct color
							_ballsPool [g].SetActive (true); //set this object to true
							_ballRigidbody[g].AddForce (0, -5000, 0); // adding some extra force down, so the ball has some higher magnitude rolling
							break; // we want this script to stop running after we instantiated a ball
						}
					}

					_intervalBall [i] = _ballIntervalTime; //reset the interval time at this line
				}

			}

			if (_intervalBall [i] > 0f) {
				_intervalBall [i] -= Time.deltaTime; //substracting time from the interval if it's above zero, making sure on this line, no balls are being spawn, while this is counting down
			}

			Color DiffuseColor = new Color (
				(_color [i].r * _audioPeer._audioBandBuffer64 [i]) * _diffuseColorMultiplier, 
				(_color [i].g * _audioPeer._audioBandBuffer64 [i]) * _diffuseColorMultiplier, 
				(_color [i].b * _audioPeer._audioBandBuffer64 [i]) * _diffuseColorMultiplier, 1); // the diffuse color is always updating, to the bandbuffers, and multiplied by the amount specified on the slider
			_materialLine [i].SetColor ("_Color", DiffuseColor); //apply the color above

			if (_audioPeer._audioBandBuffer64 [i] > _treshold) { //if the treshold specified in the slider is reached, then turn the emission on the line material on, ELSE set emission to zero
				Color EmissionColor = new Color (
					(_color [i].r * _audioPeer._audioBand64 [i]) * _emissionColorMultiplier, 
					(_color [i].g * _audioPeer._audioBand64 [i]) * _emissionColorMultiplier, 
					(_color [i].b * _audioPeer._audioBand64 [i]) * _emissionColorMultiplier, 1);
				_materialLine [i].SetColor ("_EmissionColor", EmissionColor);// apply the color above
			} else { //set emission to zero
				Color EmissionColor = new Color (0, 0, 0, 1);
				_materialLine [i].SetColor ("_EmissionColor", EmissionColor); //apply the color above
			}
		}
	}
}

