﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiate512Cubes : MonoBehaviour
{
    
    [SerializeField] GameObject _sampleCubePrefabs;
    [SerializeField] float maxScale;
    public AudioPeer audioPeer;
    GameObject[] _sampleCube  =new GameObject[512];
    // Start is called before the first frame update
        void Start()
    {
        for (int i = 0; i < 512; i++)
        {
            GameObject _instaceSampleCube = (GameObject)Instantiate (_sampleCubePrefabs);
            _instaceSampleCube.transform.position =this.transform.position;
            _instaceSampleCube.transform.parent = this.transform;
            _instaceSampleCube.name = "sample cube " + i;
            this.transform.eulerAngles = new Vector3(0, (360f/512f) * i,0);
            _instaceSampleCube.transform.position =Vector3.forward * 100;
            _sampleCube[i] = _instaceSampleCube;
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < 512; i++)
        {
            if (_sampleCube !=null){
                _sampleCube[i].transform.localScale = new Vector3 (10,(audioPeer._samplesLeft[i] *maxScale)+2,10);
            }
        }
    }
}
