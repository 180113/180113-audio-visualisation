﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParamCube : MonoBehaviour {
    // Start is called before the first frame update
    public int _band;
    public AudioPeer audioPeer;
    public float _startScale, _maxScale;
    public bool _useBuffer;
    Material material;

    void Start () {
        material = GetComponent<MeshRenderer>().materials[0];
    }

    // Update is called once per frame
    void Update () {
        if (_useBuffer) {
            Debug.Log("audio Band buffer size :" + audioPeer._audioBandBuffer[_band]);
            //Debug.Log("_band : "+ _band);
            transform.localScale = new Vector3 (transform.localScale.x, (audioPeer._audioBandBuffer[_band] * _maxScale) + _startScale, transform.localScale.z);
            Color _color =new Color(audioPeer._audioBandBuffer[_band],audioPeer._audioBandBuffer[_band], audioPeer._audioBandBuffer[_band]);
            material.SetColor("_EmissionColor", _color);
        }
        if (!_useBuffer) {

            transform.localScale = new Vector3 (transform.localScale.x, (audioPeer._audioBand[_band] * _maxScale) + _startScale, transform.localScale.z);
            Color _color =new Color(audioPeer._audioBand[_band],audioPeer._audioBand[_band], audioPeer._audioBand[_band]);
            material.SetColor("_EmissionColor", _color);
        }
    }
}