﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent (typeof (BoxCollider))]
public class UpdateBox : MonoBehaviour {
    // Start is called before the first frame update
    BoxCollider collider;
    void Start () {
        collider = GetComponent<BoxCollider> ();
    }

    // Update is called once per frame
    void Update () {

        collider.size = new Vector3(collider.size.x,collider.size.y,collider.size.z);

    }
}